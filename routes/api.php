<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'AuthController@login');

Route::get('users/{id}', 'UsersController@show');
Route::post('users', 'UsersController@store');
Route::put('users/{id}', 'UsersController@update');
Route::delete('users/{id}', 'UsersController@destroy');

Route::get('comment/{id}', 'CommentsController@index');
Route::post('comment', 'CommentsController@store');
//Route::get('comment', 'CommentsController@CommentsTeacher');

Route::get('teacher', 'TeachersController@index');
Route::get('teacher/{id}', 'TeachersController@show');
Route::post('teacher', 'TeachersController@store');
Route::put('teacher/{id}', 'TeachersController@update');

Route::get('subject', 'SubjectsController@index');
Route::get('subject/{id}', 'SubjectsController@show');
Route::post('subject', 'SubjectsController@store');
Route::put('subject/{id}', 'SubjectsController@update');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
