<?php

namespace App\Http\Controllers;

use App\Helpers\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $user = $this->checkLogin($email, $password);
        if (!$user)
            return General::makeResponse(['message' => 'Contraseña o usuario incorrecto'], 400, false);

        if (isset($tokenResponse->error))
            return General::makeResponse(['message' => 'Contraseña o usuario incorrecto'], 400, false);

        return General::makeResponse(['message' => 'Usuario válidoaaa', 'user' => $user], 200, true);
    }

    private function checkLogin($user, $password)
    {
        $auth = Auth::attempt(['email' => $user, 'password' => $password]);
        if (!$auth)
            return General::makeResponse(['message' => 'Contraseña o usuario incorrecto'], 400, false);

        return $user = Auth::user();
    }

}
