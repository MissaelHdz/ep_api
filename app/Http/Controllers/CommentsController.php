<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Helpers\General;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    public function index($id)
    {
        $comentarios = [];
        $a = Comment::all();
        foreach ($a as $comentario) {
            if($comentario->id_subject==$id)
            {
                $comentarios['comentarios'][]=[
                    $comentario
                ];
            }
        }
        if(!$comentarios)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','Comentarios'=>$comentarios], 200, true);
    }

    public function commentsTeacher(Request $request)
    {
        $date=$request->all();
        $comentarios = [];
        $a = Comment::all();
        foreach ($a as $comentario) {
            if($comentario->id_subject==$date['id_subject']&&$comentario->id_teacher==$date['id_teacher']){
                $comentarios['comentarios'][]= [
                        $comentario
                    ];
            }
        }
        if(!$comentarios)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','Comentarios'=>$comentarios], 200, true);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $comment = Comment::create($data);

        if (!$comment)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);

    }

}
