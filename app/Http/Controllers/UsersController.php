<?php

namespace App\Http\Controllers;

use App\Helpers\General;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{


    public function store(Request $request){
        $data = $request->all();
        $rules = User::$rules;
        $validator = General::validateRequest($data,$rules);
        if($validator) return $validator;
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if (!$user)
            return General::makeResponse(['message' => 'El usuario ya existe'], 400, false);
        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);
    }

    public function show($id){
        $user = User::find($id);
        if(!$user)
            return General::makeResponse(['message'=>'No se pudo completar la acción'],400,false);

        return General::makeResponse(['user'=>$user, 'message'=>'Acción realizada con éxito'],200,true);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $data = $request->all();
        if(!$user)
            return General::makeResponse(['message'=>'No se pudo completar la acción'], 400,false);

        $rules = [];
        $rules['email'] = [
            'required',
            Rule::unique('users', 'email')->ignore($user->id),
        ];

        $validator = General::validateRequest($data,$rules);
        if($validator) return $validator;

        $user->update($data);
        return General::makeResponse(['message'=>'Acción realizada con éxito'],200,true);
    }

    public function destroy($id){
        $user = User::find($id);
        if(!$user)
            return General::makeResponse(['message'=>'Error'],400,false);

        $user->delete();
        return General::makeResponse(['message'=>'Acción realizada con éxito'],200,true);
    }
}
