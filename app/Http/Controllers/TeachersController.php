<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Helpers\General;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use Illuminate\Http\Request;

class TeachersController extends Controller
{

    public function index()
    {
        $teachersAll=[];
        $teachers = Teacher::all();
        $votes = SubjectTeacher::all();
        $votesNegatives=0;
        $votesPositives=0;
        foreach ($teachers as $teacher)
        {
            foreach ($votes as $vote)
            {
                if($teacher->id==$vote->id_teacher)
                {
                    $votesNegatives+=$vote->negative_vote;
                    $votesPositives+=$vote->positive_vote;
                }
            }
            $teachersAll['teachers'][]=[
                'id'=>$teacher->id,
                'name'=>$teacher->name,
                'positive_vote'=>$votesPositives,
                'negative_vote'=>$votesNegatives
            ];
            $votesNegatives=0;
            $votesPositives=0;
        }

        return General::makeResponse( $teachersAll, 200, true);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $teacher = Teacher::create($data);
        if (!$teacher)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);
    }



    public function show($id)
    {
        $subjects = [];
        $teacher = Teacher::find($id);
        $subjectsTeacher = SubjectTeacher::all();
        $subjects['teacher'][]=[
            $teacher
        ];

        foreach ($subjectsTeacher as $subject) {
            if($subject->id_teacher==$id)
            $subjects['subjects'][]=[
                    Subject::find($subject->id_subject)
                ];
        }

        if (!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito', 'teacher' => $subjects], 200, true);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $teacher = Teacher::find($id);
        if (!$teacher)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        $teacher->update($data);
        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);

    }

    public function destroy($id)
    {
        //
    }
}
