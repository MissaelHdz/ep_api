<?php

namespace App\Http\Controllers;

use App\Helpers\General;
use App\Subject;
use App\SubjectTeacher;
use App\Teacher;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{

    public function index()
    {
        $subjects = Subject::all();
        if(!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','subjects'=>$subjects], 200, true);
    }


    public function store(Request $request)
    {
        $data=$request->all();
        Subject::create($data);

        return General::makeResponse(['message' => 'Acción realizada con éxito'], 200, true);
    }

    public function show($id)
    {
        $subjects = [];
        $subject=Subject::find($id);
        $subjectTeacher = SubjectTeacher::all();
        $subjects['subject'][]=[
            $subject
        ];
        foreach ($subjectTeacher as $subject) {
            if($subject->id_subject==$id)
                $subjects['teachers'][]=[
                    Teacher::find($subject->id_teacher)
                ];
        }

        if(!$subjects)
            return General::makeResponse(['message' => 'No se pudo completar la acción'], 400, false);

        return General::makeResponse(['message' => 'Acción realizada con éxito','subjects'=>$subjects], 200, true);
    }


    public function destroy($id)
    {
        //
    }
}
