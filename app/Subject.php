<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $fillable = [
        'name_subject','credits','key_subject'
    ];

    public function subject()
    {
        return $this->hasMany('App\Subject', 'id_teacher', 'id');
    }

}
