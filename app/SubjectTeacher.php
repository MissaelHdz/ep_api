<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    protected $table = 'subjects_teachers';

    protected $fillable = [
        'id_teacher','id_subject','information_subject','positive_vote','negative_vote'
    ];
}

